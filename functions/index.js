/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
// Configure the email transport using the default SMTP transport and a GMail account.
// For Gmail, enable these:
// 1. https://www.google.com/settings/security/lesssecureapps
// 2. https://accounts.google.com/DisplayUnlockCaptcha
// For other types of transports such as Sendgrid see https://nodemailer.com/transports/
// TODO: Configure the `gmail.email` and `gmail.password` Google Cloud environment variables.
const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword,
  },
});

// Your company name to include in the emails
// TODO: Change this to your app or company name to customize the email sent.
const APP_NAME = 'TestEmailSender';

// for date time formatting
const moment = require('moment');
// Allow cross origin requests
const cors = require('cors')({
  origin: true,
});

exports.sendContactEmail = functions.https.onRequest((req, res) => {

  return cors(req, res, () => {
    if (!req.body.recipientEmail) {
      res.status(400).send("invalid email");
    }
    
    const email = req.body.recipientEmail;
    var displayName = email;
    
    if (req.body.recipientName) {
      displayName = req.body.recipientName;
    }

    if (!req.body.customerName || !req.body.customerEmail || !req.body.customerNum || !req.body.customerFrom || !req.body.customerTo) {
      res.status(400).send('missing info');
    }

    let customerName = req.body.customerName;
    let customerEmail = req.body.customerEmail;
    let customerNum = req.body.customerNum;
    let customerFrom = req.body.customerFrom;
    let customerTo = req.body.customerTo;

    return sendContactEmail(email, displayName, customerName, customerEmail, customerNum, customerFrom, customerTo).then(() => {
      res.status(200).send();
    }, error => {
      res.status(500).send("unknown error");
    });
  });
});

// Sends a contact form email to the given user.
function sendContactEmail(email, displayName, customerName, customerEmail, customerNum, customerFrom, customerTo) {

  moment.locale('de-ch');

  const mailOptions = {
    from: `D. Uebersax <info@dominiquebersax.net>`,
    to: email,
  };

  // The user subscribed to the newsletter.
  mailOptions.subject = `Neue Bestellung`;
  mailOptions.text = 
  `Hallo ${displayName}
  
Eine neue Bestellung ist eingegangen: 
${customerName} (${customerEmail}) möchte 
${customerNum} Mitarbeiter 
von ${moment(customerFrom).format('llll')} 
bis ${moment(customerTo).format('llll')}
  
---
dominiquebersax.net
Herr
Dominique Uebersax
Brunnhofstrasse 29A
4900 Langenthal`;

  // mailOptions.html = `<h3>Hallo ${displayName}</h3>
  //                     <h4>Eine neue Bestellung ist eingegangen</h4> <br/>
  //                      <strong>${customerName}</strong> (${customerEmail}) möchte <br/>
  //                      <strong>${customerNum}</strong> Mitarbeiter <br/>
  //                      von <strong>${moment(customerFrom).format('llll')}</strong> <br/>
  //                      bis <strong>${moment(customerTo).format('llll')}</strong> <br/><br/><br/>
  //                      ---<br/>
  //                      dominiquebersax.net<br/>
  //                      Herr<br/>
  //                      Dominique Uebersax<br/>
  //                      Brunnhofstrasse 29A<br/>
  //                      4900 Langenthal`
  return mailTransport.sendMail(mailOptions).then(() => {
    return console.log('New welcome email sent to:', email);
  });
}
